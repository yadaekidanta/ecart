@if(Cache::has('categories') && is_array(Cache::get('categories')) && count(Cache::get('categories')))
	<!--section categories popular categories transparent image-->
    <section class="section-content padding-bottom popular-categories">
		<div class="container">
            <div class="outer_box px-2 py-4 px-md-4 py-md-3 bg-white shadow-sm rounded">
                <h4 class="title-section title-sec font-weight-bold">{{__('msg.popular_categories')}}</h4>
                <hr class="line">
                <div class="row">
                    @foreach(Cache::get('categories') as $i => $c)
                        <div class="col-lg-4 col-md-6 col-12 mb-2">
                            @if($c->web_image !== '')
                            <div class="item popular web">
                                <img class="category-item" src="{{ $c->web_image }}" alt="{{ $c->name ?? 'Category' }}">
                                <span class="overlay-text">
                                    <p class="text-dark title font-weight-bold name mb-0">{{ $c->name }}</p>
                                    <small class="text-muted subtitle">{{ $c->subtitle }}</small>
                                    <p class="mt-1">
                                        <a href="{{ route('category', $i) }}" class="shop-now">{{__('msg.shop_now')}} <em class="fa fa-chevron-right shopnow_arrow"></em></a>
                                    </p>
                                </span>
                            </div>
                            @else
                            <div class="item category-item-card rounded">
                                <div class="rounded-pop">
                                    <img class="category-item-rounded" src="{{ $c->image }}" alt="{{ $c->name ?? 'Category' }}">
                                </div>
                                <span class="overlay-text">
                                    <p class="text-dark title font-weight-bold name mb-0">{{ $c->name }}</p>
                                    <small class="text-muted subtitle">{{ $c->subtitle }}</small>
                                    <p class="mt-1">
                                        <a href="{{ route('category', $i) }}" class="shop-now">{{__('msg.shop_now')}} <em class="fa fa-chevron-right shopnow_arrow"></em></a>
                                    </p>
                                </span>
                            </div>
                            @endif
                        </div>
                    @endforeach
                </div>
            </div>
		</div>
    </section>
    <!--end section categories-->
@endif