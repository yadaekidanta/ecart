@if(Cache::has('offers') && is_array(Cache::get('offers')) && count(Cache::get('offers')))
    @foreach(Cache::get('offers') as $o)
        @if(isset($o->image) && trim($o->image) !== "")
            <section class="section-content banneradvertise spacingrm">
                <div class="container">
                    <article class="padding-bottom">
                        <div class="banner_outer">
                             <img src="{{ $o->image }}" class="w-100" alt="offer">
                        </div>
                    </article>
                </div>
            </section>
        @endif
    @endforeach
@endif