@include("themes.$theme.common.msg")
<section class="section-content footerfix padding-bottom mt-5">
    <a href="#" id="scroll"><span></span></a>
    <div class="container">
        <div class="card px-2 py-4 px-md-4 py-md-3 bg-white shadow-sm rounded mb-4">
            <div class="row">
                <div class="col-md-4 col-sm-4 col-12 text-center">
                    <span class="icon dark"><em class="fa fa-chevron-circle-right delivery-icon"></em>
                        {{ __('msg.delivery') }}</span>
                </div>
                <div class="col-md-4 col-sm-4 col-12 text-center payment-icon">
                    <span class="icon dark"><em class="fa fa-chevron-circle-right"></em> {{ __('msg.address') }}</span>
                </div>
                <div class="col-md-4 col-sm-4 col-12 text-center payment-icon">
                    <span class="icon dark"><em class="fa fa-chevron-circle-right"></em> {{ __('msg.payment') }}</span>
                </div>
            </div>
        </div>
        <div class="row">
           <div class="col-md-12 col-12">
                <div id="data-step1" class="account-content px-2 py-4 px-md-4 py-md-3 bg-white shadow-sm rounded" data-temp="tabdata">
                    <div class="cart-main-content py-2 py-lg-5">
                        <div class="container">
                            <h3 class="cart-page-title">{{ __('msg.order_summary') }}</h3>
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-12">
                                    <div class="table_description">
                                        <div class="cart_page-content">
                                            <table aria-describedby="cart-table">
                                                <thead>
                                                    <tr class="cart-header">
                                                        <th scope="col" class="header_product_thumb">{{__('msg.image')}}</th>
                                                        <th scope="col" class="header_product_name">{{__('msg.product')}}</th>
                                                        <th scope="col" class="header_product-price">{{__('msg.price')}}</th>
                                                        <th scope="col" class="header_product_quantity">{{__('msg.qty')}}</th>
                                                        <th scope="col" class="header_product_total">{{__('msg.subtotal')}}</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if(isset($data['cart']) && is_array($data['cart']) && count($data['cart']))
                                                    @foreach($data['cart'] as $p)
                                                    @if(isset($p->item))
                                                    <tr>
                                                        <td class="header_product_thumb"><a href="#"><img src="{{ $p->item[0]->image }}" alt=""></a></td>
                                                        <td class="header_product_name">
                                                            <a href="#">{{ strtoupper($p->item[0]->name) ?? '-' }}</a>
                                                            <p class="small text-muted text-center">
                                                                {{ get_varient_name($p->item[0]) }}
                                                                @if(intval($p->item[0]->discounted_price))
                                                                ({{intval($p->item[0]->discounted_price)}} X {{($p->qty ?? 1)}})
                                                                @else
                                                                ({{intval($p->item[0]->price)}} X {{($p->qty ?? 1)}})
                                                                @endif
                                                                <br>{{ __('msg.tax')." (".$p->item[0]->tax_percentage  }}%
                                                                {{ $p->item[0]->tax_title  }})
                                                            </p>
                                                        </td>
                                                        <td class="header_product-price">@if(intval($p->item[0]->discounted_price))
                                                            @if (isset($p->item[0]->tax_percentage) && $p->item[0]->tax_percentage >
                                                            0)
                                                            {{ $p->item[0]->discounted_price+($p->item[0]->discounted_price*$p->item[0]->tax_percentage/100) ?? '' }}
                                                            @else
                                                            {{ $p->item[0]->discounted_price ?? '' }}
                                                            @endif
                                                            @else
                                                            @if (isset($p->item[0]->tax_percentage) && $p->item[0]->tax_percentage >
                                                            0)
                                                            {{ $p->item[0]->price+($p->item[0]->price*$p->item[0]->tax_percentage/100) ?? '' }}
                                                            @else
                                                            {{ $p->item[0]->price ?? '' }}
                                                            @endif
                                                            @endif
                                                        </td>
                                                        <td class="header_product_quantity">
                                                            {{ $p->qty }}
                                                        </td>
                                                        <td class="header_product_total">@if(intval($p->item[0]->discounted_price))
                                                            @if (isset($p->item[0]->tax_percentage) && $p->item[0]->tax_percentage >
                                                            0)
                                                            {{ ($p->item[0]->discounted_price+($p->item[0]->discounted_price*$p->item[0]->tax_percentage/100)) * ($p->qty ?? 1) }}
                                                            @else
                                                            {{ $p->item[0]->discounted_price * ($p->qty ?? 1) }}
                                                            @endif
                                                            @else
                                                            @if (isset($p->item[0]->tax_percentage) && $p->item[0]->tax_percentage >
                                                            0)
                                                            {{ ($p->item[0]->price+($p->item[0]->price*$p->item[0]->tax_percentage/100)) * ($p->qty ?? 1) }}
                                                            @else
                                                            {{ $p->item[0]->price * ($p->qty ?? 1) }}
                                                            @endif
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @endif
                                                    @endforeach
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="cart_submit">
                                            <a href="{{ route('shop') }}" class="btn cart_shopping"><em class="fas fa-angle-double-left"></em>&nbsp;&nbsp;{{__('msg.continue_shopping')}}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-12 ">
                              <div class="discount-code-wrapper">
                                 <div class="title-wrap">
                                    <h4 class="cart-bottom-title section-bg-gray">{{__('msg.use_coupan_code')}}</h4>
                                 </div>
                                 @if(intval($data['coupon'] ?? 0))
                                 <div class="form-group" id='couponAppliedDiv'>
                                    <label class="title-sec">{{__('msg.coupon_code')}}</label>
                                    <div class="alert alert-success">{{ $data['coupon']['promo_code_message'] }}</div>
                                    <div class="input-group">
                                       <input type="text" class="form-control" value="{{ $data['coupon']['promo_code'] }}" disabled="disabled" placeholder="Coupon code">
                                       <span class="input-group-append">
                                       <a href="{{ route('coupon-remove') }}" class="btn btn-danger" id='removeCoupon'>x</a>
                                       </span>
                                    </div>
                                 </div>
                                 @endif
                                 <div class="discount-code">
                                    <p>{{__('msg.enter_your_coupon_code_if_you_have_one')}}</p>
                                    <form action="{{ route('coupon-apply') }}" method="POST" class='ajax-form {{ intval($data['coupon'] ?? 0) ? 'address-hide' : '' }}' id='couponForm'>
                                       <input type="hidden" name="total" value="{{ $data['total'] }}">
                                       <div class='formResponse'></div>
                                       <div class="input-group">
                                          <input type="text" class="form-control" name="coupon" value="{{ $data['coupon']['promo_code'] ?? '' }}" placeholder="Coupon code">
                                          <span class="input-group-append">
                                             <button class="btn btn-primary">{{__('msg.apply_coupan')}}</button>
                                          </span>
                                       </div>
                                    </form>
                                 </div>
                              </div>
                           </div>
                                <div class="col-lg-6 col-md-6 col-12 ">
                                    <div class="grand-total-content">
                                        <div class="title-wrap">
                                            <h4 class="cart-bottom-title section-bg-gary-cart">{{__('msg.cart_total')}}</h4>
                                        </div>
                                        <h5>{{__('msg.subtotal')}} : <span>{{ get_price($data['subtotal'] ?? '-') }}</span></h5>
                                        @if(isset($data['tax_amount']) && floatval($data['tax_amount']))
                                        <h5>{{__('msg.tax')}} {{ $data['tax'] ? $data['tax']."%" : '' }} :
                                            <span>+{{ get_price($data['tax_amount']) }}</span>
                                        </h5>
                                        @endif
                                        @if(isset($data['coupon']['discount']) && floatval($data['coupon']['discount']))
                                        <h5>{{__('msg.discount')}} :
                                            <span>-{{ get_price(sprintf("%0.2f",$data['coupon']['discount'])) ?? '-' }}</span>
                                        </h5>
                                        @endif
                                        @if(isset($data['saved_price']) && floatval($data['saved_price']))
                                        <h5>{{__('msg.saved_price')}} :
                                            <span>{{ get_price($data['saved_price']) }}</span>
                                        </h5>
                                        @endif
                                        <a href="{{ route('checkout-address') }}">{{__('msg.proceed_to_address')}}&nbsp;&nbsp;<em class="fas fa-angle-double-right"></em></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
